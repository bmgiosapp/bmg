# BMG App

This is a web application designed to showcase various apps developed by BMG. The application displays a list of apps along with their titles and subtitles. Users can click on the "App Store" button to navigate to the respective app's page on the App Store.

## Features

- Displays a list of apps developed by BMG.
- Each app entry includes a title, subtitle, and a button to navigate to the App Store.
- The current time is displayed at the bottom of the page and updates in real-time.
- The layout is responsive and supports horizontal scrolling for the list of apps.

## Technologies Used

- HTML
- CSS
- JavaScript

## License

This project is licensed under the [MIT License](LICENSE).
