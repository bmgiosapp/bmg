document.addEventListener("DOMContentLoaded", () => {
    if (localStorage.getItem("darkTheme")) {
        document.body.classList.add("dark-mode");
    }
    updateThemeIcon();

    showTime();
    setInterval(showTime, 1000);
    displayData();
    fetchInfo();
});

function showTime() {
    const now = new Date();
    const days = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
    ];
    const day = days[now.getDay()];
    const options = {
        day: "2-digit",
        month: "2-digit",
        year: "numeric",
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
        hour12: false,
    };
    const date = now.toLocaleDateString("en-GB", options);
    const dateTimeString = `${day}, ${date}`;
    document.getElementById("currentTime").innerHTML = dateTimeString;
}

function toggleTheme() {
    document.body.classList.toggle("dark-mode");
    if (document.body.classList.contains("dark-mode")) {
        localStorage.setItem("darkTheme", true);
    } else {
        localStorage.removeItem("darkTheme");
    }
    updateThemeIcon();
}

function updateThemeIcon() {
    const themeToggleButton = document.querySelector(".theme-toggle-btn");
    themeToggleButton.value = document.body.classList.contains("dark-mode")
        ? "🌞"
        : "🌔";
}