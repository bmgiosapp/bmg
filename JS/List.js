// List Setup
let myArr = [
    {
        title: "The Alchemy Quest:",
        subTitle: "Elemental Fusion",
        link: "https://apps.apple.com/app/id6480043018",
        image: "https://is1-ssl.mzstatic.com/image/thumb/Purple221/v4/77/c4/d5/77c4d529-d5c0-3dd7-52aa-ecdbb41062f3/AppIcon-0-1x_U007epad-0-0-85-220-0.jpeg/230x0w.webp",
    },
    {
        title: "MineSweeper Ensemble:",
        subTitle: "Conquer Universe of Variants",
        link: "https://apps.apple.com/app/id6498897615",
        image: "https://is1-ssl.mzstatic.com/image/thumb/Purple221/v4/fd/bd/a5/fdbda5e8-1234-81e7-c0b0-cdc4e6c66665/AppIcon-0-1x_U007emarketing-0-10-0-sRGB-85-220-0.png/230x0w.webp",
    },
    {
        title: "Chain Reaction Remix:",
        subTitle: "Fission Fusion Odyssey",
        link: "https://apps.apple.com/app/id6502253960",
        image: "https://is1-ssl.mzstatic.com/image/thumb/Purple211/v4/c2/6d/d3/c26dd366-8b7a-a35c-f4b9-c5f9e9f5988b/AppIcon-0-1x_U007epad-0-0-85-220-0.jpeg/230x0w.webp",
    },
    {
        title: "Make Square Game:",
        subTitle: "Dots and Lines Mastery",
        link: "https://apps.apple.com/app/id6502521998",
        image: "https://is1-ssl.mzstatic.com/image/thumb/Purple221/v4/38/f8/c5/38f8c588-eaf8-0b21-819a-4f49ba7cef53/AppIcon-0-1x_U007epad-0-0-85-220-0.jpeg/230x0w.webp",
    },
    {
        title: "TicTacToe: Classic to Extreme",
        subTitle: "Classic + AI + Extreme Mode",
        link: "https://apps.apple.com/app/id6502718311",
        image: "https://is1-ssl.mzstatic.com/image/thumb/Purple211/v4/fe/80/29/fe802956-f846-5b4d-d54e-8c710db8f3d3/AppIcon-0-1x_U007epad-0-85-220-0.jpeg/230x0w.webp",
    },
    {
        title: "Pentago Classic:",
        subTitle: "Strategize, Rotate, Win",
        link: "https://apps.apple.com/app/id6502961672",
        image: "https://is1-ssl.mzstatic.com/image/thumb/Purple221/v4/2e/41/0b/2e410bef-88a6-abeb-8bb6-76b9b6dbd75a/AppIcon-0-1x_U007epad-0-85-220-0.jpeg/230x0w.webp",
    },
    {
        title: "Quarto Master:",
        subTitle: "Unique Turn-Based Game",
        link: "https://apps.apple.com/app/id6503365033",
        image: "https://is1-ssl.mzstatic.com/image/thumb/Purple221/v4/63/85/7c/63857c7f-ef4a-bfe4-a551-32433f14ca1a/AppIcon-0-1x_U007epad-0-85-220-0.jpeg/230x0w.webp",
    },
    {
        title: "2048 Fusion Universe:",
        subTitle: "Various Grid Sizes & Themes!",
        link: "https://apps.apple.com/app/id6514292904",
        image: "https://is1-ssl.mzstatic.com/image/thumb/Purple211/v4/3d/eb/21/3deb21e6-4527-20f5-b811-0a129eaf732c/AppIcon-0-1x_U007epad-0-85-220-0.jpeg/230x0w.webp",
    }
];

function displayData() {
    let target = document.querySelector("#target");
    target.innerHTML = "";
    myArr.forEach(function (item) {
        let div = document.createElement("div");
        div.className = "item-container";

        let backgroundImage = document.createElement("div");
        backgroundImage.className = "bgImage";
        if (item.image) {
            backgroundImage.style.backgroundImage = `url(${item.image})`;
        } else {
            backgroundImage.innerHTML = '<i class="fas fa-spinner fa-spin fa-5x" style="color: white;"></i>';
            backgroundImage.style.display = 'flex';
            backgroundImage.style.justifyContent = 'center';
            backgroundImage.style.alignItems = 'center';
        }
        div.appendChild(backgroundImage);

        let title = document.createElement("h2");
        title.className = "title";
        title.appendChild(document.createTextNode(item.title));
        div.appendChild(title);

        let subTitle = document.createElement("h3");
        subTitle.className = "subTitle";
        subTitle.appendChild(document.createTextNode(item.subTitle));
        div.appendChild(subTitle);

        let appleButton = document.createElement("button"); // Use button element instead of input
        appleButton.className = "appleButton";
        appleButton.onclick = function () {
            window.open(item.link, "_blank");
        };

        let icon = document.createElement("i");
        icon.className = "fab fa-apple";
        appleButton.appendChild(icon);

        div.appendChild(appleButton);

        target.appendChild(div);
    });
}

function fetchInfo() {
    const Id =
        "2PACX-1vQa_NF0omBywz8BM0tYaj6HRz1dFy3C7lSPOrjBd5nU14YBHDY3Zsm9pvoQ6J1_kVeuRvNYGcg7Wg9U";
    const googleSheetUrl = `https://docs.google.com/spreadsheets/d/e/${Id}/pub?gid=0&single=true&output=csv`;
    fetch(googleSheetUrl)
        .then((response) => response.text())
        .then((csvText) => {
            const data = csvToArray(csvText);
            myArr = data.map((item) => ({
                title: item.Title || "No Title",
                subTitle: item.SubTitle || "No Subtitle",
                link: item.Link || "https:/",
                image: item.Image || "apple.png",
            }));
            displayData();
        })
        .catch((error) =>
            console.error("Error fetching Google Sheets data:", error)
        );
}

function csvToArray(str, delimiter = ",") {
    const rows = str.split("\n");
    const headers = rows
        .shift()
        .split(delimiter)
        .map((header) => header.trim());

    return rows.map((row) => {
        let current = "";
        let inQuotes = false;
        let obj = {};

        for (let char of row) {
            if (char === delimiter && !inQuotes) {
                let val = current.trim();
                if (val === "") {
                    console.log("X", obj, val, row);
                    return null;
                }
                obj[headers[Object.keys(obj).length]] = val;
                current = "";
            } else if (char === '"') {
                inQuotes = !inQuotes;
            } else {
                current += char;
            }
        }

        let val = current.trim();
        if (val !== "") {
            obj[headers[Object.keys(obj).length]] = val;
        }

        console.log(obj);
        return obj;
    })
    .filter((obj) => obj !== null);
}
